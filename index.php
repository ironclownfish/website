<?php session_start() ?>

<!DOCTYPE html>
<html>
<head>
<meta charset = "utf-8">
<meta http-equiv="X-UA-Compatible" content="IE-edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="jquery-1.11.0.min.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="d3.min.js"></script>
<script type="text/javascript" src="https://www.java.com/js/deployJava.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="bootstrap-3.1.1-dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="theme.css">
<link rel="stylesheet" type="text/css" href="indexStyle.css">
<link rel="stylesheet" type="text/css" href="justified-nav.css">
<link rel="stylesheet" type="text/css" href="simple-sidebar.css">
</head>

<body>
<div id='topnavdiv' class="navbar navbar-inverse navbar-fixed-top" role="navigation" style='margin: 0;'>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-darget=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Nicholas Gydé</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li id='homenav' class='active'><a href="#home">Home</a></li>
                <li id='resumenav'><a href="#resume">Résumé</a></li>
		<li id='edunav'><a href="#education">Education</a></li>
		<li id='projectsnav'><a href="#projects">Projects</a></li>

		<li id='interestsnav' class='construction'><a href="#projects">Interests</a></li>
		<li id='contactnav'><a href="#contact">Contact</a><li>
		</div>
		</div>
		</div>
		<div id='homediv' style='position:absolute; overflow:hidden; height:100%; width:100%; background-image:url(layer0.png); background-repeat:repeat; background-position:left bottom; top:0;'> 	
		<div id='homelayer2' style='width:1400px; height:327px; background: url(layer2.png); z-index:2;'></div>
		<div id='homelayer1' style='width:inherit; height:218px; background: url(layer1.png); background-repeat:no-repeat; z-index:1;'>
		</div>
		</div>
   		<div id='resumediv' style='margin-top:30px; height:100%; padding-top:10px;' hidden>
<!--		  <div id='sidebar-wrapper'>
		    <ul class='sidebar-nav'>
		      <li id='pdfnav' class='active'> <a href='#'>pdf</a></li>
		      <li id='edunav'> <a href='#'>Education</a></li>
		    </ul>
		  </div>-->
		<div style='padding-left:8px; padding-top:6px;'>
		<div id='pdfdiv' style='width:1000px; height:100%; min-height:600px; margin-left:auto; margin-right:auto;'>

		<iframe src="https://docs.google.com/file/d/0ByMus623Kx0id3Iydld2blVDZFU/preview" width="100%" height="94%" style='height: expression(this.scrollHeight < 501 ? "500px" : "auto");'></iframe>
		</div>
		</div>
		</div>
		<div id='chart' style='position: absolute; margin-left: 20%;' hidden></div>
		<div id='projectsdiv' style='margin-top:30px; height:100%;' hidden>
		<ul id='projectsnav' class='nav nav-justified'>
		<li id='reuprojnav'class='active'><a href='#projects'>REU Webapp</a></li>
		<li id='chessprojnav'><a href='#projects'>Chess Engine</a></li>
		<li id='fpgaprojnav'><a href='#projects'>Microscope</a></li>
		<li id='shellprojnav'><a href='#projects'>Custom Shell</a></li>
		<li id='spaceprojnav'><a href='#projects'>Gravity Sandbox</a></li>
		<li id='websiteprojnav'><a href='#projects'>This Website</a></li>
		</ul>
		<div id='ontologydiv' style='height:86%'>	
		<iframe id='ontologyframe' style='position: absolute; top:101px; height:80%; width:100%; margin:0;' src='http://aimlab.cs.uoregon.edu/services/skti-ontogen'></iframe>
	<!--	<p>
From Winter 2013 through Spring 2014, I participated in an REU that focused on the design and programming of a front-end web application used in machine learning research.
<br> The application I worked on is used for constructing a reference alignment between two ontologies (knowledge bases). My job, along with another undergraduate, was to design and implement a user interface for inputting “complex changes” to an ontology. This includes things like mapping one class to many, mapping a class to a property, and much more.
<br> Over the course of the project, I went from knowing nothing about web development, to using HTML, CSS, and Javascript proficiently. We used JQuery and D3 extensively.

		</p>-->	
		<!--<div class='container' style='position: absolute'>
		<p>Throughout my REU, I and another undergraduate designed and implemented the "Complex Change" interface, as well as added d3 effects such as auto-zoom and tree collapsing to the ontology graph.
		</p>
		</div>-->
		</div>
		  <div id='chessdiv' class='container' hidden>
		    <!--<APPLET ARCHIVE='Chess.jar' CODE='Chess.class'></APPLET>-->
		    <div id='chessdescription' class='container' style='padding:20px; width:50%; float:left;'>
		    <p>I always always wanted to make a chess engine. 
		    Making things that think (or appear to think) on their own is the most exciting area of computer science to me. In fact, I think it's really all of computer science. 
		    After I took my first class in artificial intelligence, I decided to finally try making a chess engine over Winter break (2013).</p>
		    <p>It was very very hard: even harder than the hardness I expected. 
		    I struggled a lot at first just trying to get everything to follow the rules of chess. For instance, here are a few edge cases I listed in the source code. Each one is something that had gotten me stuck at one point:</p>
                    <p style='font-family:"Courier New"; color:green;'>
/*<br>
* Some edge cases for anyone making a chess engine:<br>
* - en passant<br>
* - Discovered check<br>
* - Discovered check due to en passant<br>
* - Check delivered by castling<br>
* - Check delivered by promotion<br>
* - Because of promotions, a given move does not have a well-<br>
* defined outcome. <br>
*<br>
*/
</p>
		    </div>
		    <div class='container' style='padding:20px;'>
		      <p>Nonetheless, after a couple weeks I managed to complete a GUI and restrict the pieces to only legal moves, as well as detect checkmates, checks, and stalemates.</p>
		      <p>It took the rest of Winter break to build the engine A.I. which uses an implementation of the negamax algorithm with alpha-beta pruning. It is a very simple opponent, which tries to maximize its material over the next 6 ply. (The search depth is easy to change in the source code.) Checkmates are treated as maximally valuable states. It also uses a common chess heuristic called the "history heuristic," where moves that have caused an alpha/beta cutoff before are searched first, since they are likely to cause a cutoff again.</p>
		      <p>The A.I. also has a simple opening book that I typed in by hand in a sort of Scheme-esque way, so it's easy to add to. It's parsed at runtime, and opening lines chosen at random. The A.I. also has a list of opening moves that are usually good to play at some time or other, and can use those when it's not on a book line.</p>
		      <p>If you would like to try out the game, click "Download" below! If you plan to play against the A.I., be sure to turn it on in the "options" menu. If you want to edit the program, or make your own A.I., there is also a link to clone the source code. Beware, though: the whole thing was thrown together in a month by only me, so it's not all graceful.
		      <p>Now that I have studied artificial intelligence a lot more, I plan to make a new A.I. for this game. It would be interesting to make a learning engine that improves by playing itself many many times and observing which strategies tend to be successful.
		    </div>
		    <div id='chesslinks' class='container' style='padding:20px;'>

<a href='https://bitbucket.org/ironclownfish/chess/src/6cbd91d17f050ea1cbfd5b1578727b2332fa0120?at=master' class="btn btn-primary btn-large"><i class="glyphicon glyphicon-white glyphicon-file"></i> Clone Source</a>

<a id='chessrepolink' href='Chess.jar' class="btn btn-primary"><span class="glyphicon glyphicon-cloud-download"></span> Download</a>	
		    </div>
		    <div>
		    </div>
<!--		    <script>
		      var attributes = {code:'AppletDriver.class', archive:'ChessApplet.jar', width:800,height:1000};
		      var parameters = {fontSize:16, permissions:'sandbox'};
		      var version = '1.7';
		      deployJava.runApplet(attributes, parameters, version);
		    </script>-->
		  </div>
		  <div id='shelldiv' hidden>
<!--		    <div sytle='position:absolute; margin:30px; height:580px; width:640px; float:left;'><iframe id='shellframe' style='width:640px; height:580px;' src='http://bellard.org/jslinux/'></iframe></div>-->
		  <div style='margin:30px;'>
		  <p>This shell was the main project for my Operating Systems class. 
		  I put it up here to show employers that I can do low-level C programming. This shell makes system calls directly, and does not use library functions. Since it is interfacing directly with the kernel, I recommend keeping it safely inside the virtual machine I provide here.
		  <p>I'd really like to emulate the shell in the browser for people to try, but I have to figure out how to set up some kind of virtual machine on my server with a terminal ebedded in this page. 
		  For now, if you would like to try the shell, please run Ubuntu virtual machine available for download below (about 3GB).
		  The VM password is 415s12, and the shell executable (a.out) is on the desktop.
		  </p><p>
		  Unfortunately I can't provide the source code. My instructor assigns the same project every term. It's a very difficult assignment, so he goes to great lengths to prevent cheating. I wouldn't want to get anyone in trouble by sharing my solution. </p>
<a id='shelldownload' href='Ubuntu10-cis415.vmdk' class="btn btn-primary"><span class="glyphicon glyphicon-cloud-download"></span> Download</a>
		  </div>
		  </div>
		  <div id='fpgadiv' hidden>
		    <div id='mictxt1' style='margin-top:30px; width:40%; float:left;' class='container'>
		      <p>Throughout most of 2013, I participated in the University of Oregon's advanced physics projects lab. 
		      Students who are granted permission to work in this lab are given a long-term project: in my case, building an atomic force microscope nearly from scratch (a metal frame and a moving sample stage were already in place).
		      The lab is intended to be mostly self-guided, so my lab partner and I had to figure everything out for ourselves, given only the general idea of how existing atomic force microscopes work.</p>
		      <p>The classic AFM design is similar to a phonograph. 
		      As seen in this animation (made by my lab partner Jared), a tiny needle is touched to a surface, and its resulting deflection is amplified to reveal the microscopic shape of the surface. The main difference is that a phonograph amplifies its needle movements sonically, but in an AFM the movements are amplified by a laser (optically). 
		      A detector measures how much the reflected laser beam has moved, and this data can be used to build up a 3D model of the surface!</p>
		      <a href='#' class='nextbtn' onclick="
		        $('#mictxt1, .micimg1').fadeOut(function() {
			$('#mictxt2, .micimg2').fadeIn();	
			});	
		      ">Next</a>
		    </div>
		    <div id='mictxt2' class='container' style='margin-top:30px; width:30%; float:left;' hidden>
		      <p>Here is a more detailed diagram (also created by the impeccable Jared) of what became our plan.
		      We designed an array of lenses in a tube that would focus the laser to as small a point as possible. We would use a mirror to guide the laser toward the needle at an angle. Then, when the laser reflected off the needle, a photodetector could be placed at a complementary position.</p>
		      <p>The intermediate mirror was necessary to zigzag the laser's path,thereby lengthening it. 
		      We wanted to converge the beam gradually over a long distance because doing so stretches the <a href='http://en.wikipedia.org/wiki/Rayleigh_range'>Rayleigh length</a>, the useful, focused part of the beam.
		      </p>
		      <a href='#' class='backbtn' onclick=" 
		        $('#mictxt2, .micimg2').fadeOut(function() {
			$('#mictxt1, .micimg1').fadeIn();
			});
		      ">Back</a>
		      <a href='#' class='nextbtn' onclick="
		        $('#mictxt2, .micimg2').fadeOut(function() {
			$('#mictxt3, .micimg3').fadeIn();	
			});	
		      ">Next</a>
		    </div>
		    <div id='mictxt3' class='container' style='margin-top:30px; width:40%; float:left;' hidden>
		      <p>Alas, after trying the mirror approach, we found that reflecting the beam at an angle caused it to smear. 
		      A smeared beam is a larger beam, and the triangular needles we were aiming for (seen here, close up) were only about 20 microns wide. We decided we would have to aim the laser directly at a needle to eliminate the smearing. 
		      </p><p>The focused laser dot can also be seen in this photo, a few microns away from the needle tips.</p>
		      <a href='#' class='backbtn' onclick=" 
		        $('#mictxt3, .micimg3').fadeOut(function() {
			$('#mictxt2, .micimg2').fadeIn();
			});
		      ">Back</a>
		      <a href='#' class='nextbtn' onclick="
		        $('#mictxt3, .micimg3').fadeOut(function() {
			$('#mictxt4, .micimg4').fadeIn();	
			});	
		      ">Next</a>
		      </div>
		    <div id='mictxt4' class='container' style='margin-top:30px; width:100%; margin-bottom:20px;' hidden>
		      <p style='width:50%; float:left; padding:8px;'>Meanwhile, I also worked on software to control the microscope and retreive measurements from it. 
		      In our particular microscope, the needle was not dragged over the sample, but the sample was dragged under the needle (once again mimicking a phonograph). 
		      Three piezoelectric actuators, which use crystals that expand under voltage, could move the sample by microscopic amounts in any direction. 
		      The software had to make these actuators zigzag the sample under the needle, so that the needle would contact every point once. 
		      I also had to create software to send the measurements to a PC via an interrupt process, and design a user interface for running scans of different sizes and resolutions.</p>
		      <p style='padding:8px; margin-bottom:30px;'>Below is an image of the source code I made for moving the sample beneath the needle.
		      It's written in LabVIEW, a graphical programming language that involves drawing logic circuits by hand. 
		      We then compiled these circuit-like programs onto a device called a <a href='http://en.wikipedia.org/wiki/Field-programmable_gate_array'>Field Programmable Gate Array</a> (FPGA). The FPGA is a small analog computer that can have circuits magnetically printed on it via a PC. 
		      The advantage of using an FPGA is that its analog nature allows it to run an entire program in one clock cycle.
		      This allowed our microscope to achieve a sampling rate of about 1 MHz</p>
		      <a href='#' class='backbtn' onclick=" 
		        $('#mictxt4, .micimg4').fadeOut(function() {
			$('#mictxt3, .micimg3').fadeIn();
			});
		      ">Back</a>
		      <a href='#' class='nextbtn' onclick="
		        $('#mictxt4, .micimg4').fadeOut(function() {
			$('#mictxt5, .micimg5').fadeIn();	
			});	
		      ">Next</a>
		    </div>
		    <div id='mictxt5' class='container' style='margin-top:30px; width:100%; float:left; margin-bottom:20px;' hidden>
		      <p style='width:50%; float:left; padding:20px;'>After months in the lab, we finally got the software, laser, detector, piezo actuators, FPGA, and hand-made wiring (including batteries velcroed inside a magnetically shielded circuit board case), working together. During the last week of Spring term, just before, summer, we were able to take our first scans!</p>
		      <p style='padding:20px; margin-bottom:30px;'>Below (left) are some 3D images produced by the atomic force microscope. They show the tiny concentric ridges on the scope's aluminum sample platform (seen on the right, slightly obscured), which was being scanned empty.</p>
		      <a href='#' class='backbtn' onclick=" 
		        $('#mictxt5, .micimg5').fadeOut(function() {
			$('#mictxt4, .micimg4').fadeIn();
			});
		      ">Back</a>
		      <a href='#' class='nextbtn' onclick="
		        $('#mictxt5, .micimg5').fadeOut(function() {
			$('#mictxt6, .micimg6').fadeIn();	
			});	
		      ">Next</a>
		    </div>
		    <div id='mictxt6' class='container' style='margin-top:30px; width:100%; float:left; margin-bottom:20px;' hidden>
		    <p>The week after we took our first scans, Jared graduated. 
		    I continued working on the AFM alone, changing its physical design and optimizing the software. 
		    I eventually changed the lense array to focus the laser to an even smaller point over a very short distance, and aimed it almost vertically downward. Now the AFM lives on its own table in the projects lab, waiting for new lab members to make it better.</p>
		      <a href='#' class='backbtn' onclick=" 
		        $('#mictxt6, .micimg6').fadeOut(function() {
			$('#mictxt5, .micimg5').fadeIn();
			});
		      ">Back</a>
		    </div>
		    <div>
		      <img class='micimg1' src='images/scanimation.gif'/>
		      <img class='micimg2' src='images/plan.png' hidden/>
		      <img class='micimg3' width='59%' src='images/AFMtips.jpg' hidden/>
		      <img class='micimg4' src='images/oldScanSoftware.png' hidden/>
		      <img class='micimg5' width='33.33%' style='border-width:2px; padding: 1em 1em 0;' src='images/scan.png' hidden/>
		      <img class='micimg5' width='66%' style='float:right;' src='images/AFMsamplestage.jpg' hidden/>
		      <img class='micimg5' width='33.33%' style='padding: 1em 1em 0;' src='images/movingscan.gif' hidden/>
		      <img class='micimg6' width='50%' style='float:left;'  src='images/AFMLaserSetup.jpg' hidden/>
		      <img class='micimg6' width='50%' src='images/setup2.jpg' hidden/>
		    </div>
		  </div>
		  <div id='spacediv' hidden>
		    <div class='container' style='width: 50%; margin-top:40px;'>
		     <p>I had to do some kind of physics project for Analytical Mechanics, so I stapled together the two coolest things I knew of that already existed: computers and space. 
		     The result was this simple solar system simulator that includes a basic custom scripting language for affecting it.
		     To open the scripting window in the app, press ENTER.</p>
		     <p>Watch out. If you get a celestial body below its Schwarzschild radius, black holes will happen.</p> 
		    </div>
		    <div class='container' style='width: 50%;'>
<!--<a href='' class="btn btn-primary"><i class="glyphicon glyphicon-white glyphicon-file"></i> Clone Source</a>-->
<a id='spacerepolink' href='SolarSystemSim.jar' class="btn btn-primary"><span class="glyphicon glyphicon-cloud-download"></span> Download</a>	
		    </div>
		  </div>
		  <div id='websitediv' hidden>
		    <div class='container' style='margin-top:40px'>
		    <p>My website uses a combination of bootstrap, jQuery, and D3. I created it to provide information to potiential employers and demonstrate my web programming ability. It's also for fun.</p>
		    </div>
		  </div>
		</div>
		<div id='interestsdiv' class='container' style='margin-top:30px;' hidden>
		  <ul id='interestsnav' class='nav nav-justified'>
		    <li id='musicnav' class='active'><a href='#'>Music</a></li>
		    <li id='astronav'><a href='#'>Astronomy</a></li>
		    <li id='flynav'> <a href='#'>Flight</a></li>
		    <li id='gamesnav'><a href='#'>Games</a></li>
		  </ul>
		  <div id='musicdiv'>
		  </div>
		</div>
		<div id='contactdiv' style='margin-top:30px;' class='container' hidden>

		<div id='contactinfo' class='infobox' style='margin-top:30px;'> 
		<p><span class="glyphicon glyphicon-envelope"></span> nicholas@nicholasgyde.com</p>
		<p><span class="glyphicon glyphicon-phone"></span> 541.321.2221</p>
<a href="http://www.linkedin.com/pub/nicholas-gyd%C3%A9/23/769/3b9" style="text-decoration:none;"><span style="font: 80% Arial,sans-serif; color:#0783B6;"><img src="https://static.licdn.com/scds/common/u/img/webpromo/btn_in_20x15.png" width="20" height="15" alt="View Nicholas Gydé's LinkedIn profile" style="vertical-align:middle" border="0">View Nicholas Gydé's profile</span></a>
		</div>


		<!-- CONTACT FORM -->
<div id="contact-form" class="clearfix">
<h1>Send me an e-mail!</h1>
<?php
  $cf = array();
  $sr = false;
  if(isset($_SESSION['cf_returndata'])){
    $cf = $_SESSION['cf_returndata'];
    $sr = true;
  }
?>
<ul id="errors" class="<?php echo ($sr && !$cf['form_ok']) ? 'visible' : ''; ?>">
<li id="info">There were some problems with your form submission:</li>
<?php 
if(isset($cf['errors']) && count($cf['errors']) > 0) :
foreach($cf['errors'] as $error) :
?>
<li><?php echo $error ?></li>
<?php
endforeach;
endif;
?>
</ul>
<p id="success" class="<?php echo ($sr && $cf['form_ok']) ? 'visible' : ''; ?>">Thanks!</p>
<form method="post" action="process.php">

<label for="email">Your Email: <span class="required">*</span></label>
<input type="email" id="email" name="email" value="" placeholder="reply_email@example.com" required="required" />

<label for="subject">Subject: </label>
<input type="subject" id="subject" name="subject" value="" placeholder="cool stuff" required="required" />
</select>

<label for="message">Message: <span class="required">*</span></label>
<textarea id="message" name="message" placeholder="Message text..." required="required" data-minlength="20"></textarea>

<span id="loading"></span>
<input type="submit" value="Send" id="submit-button"/>
<p id="req-field-desc"><span class="required">*</span> indicates a required field</p>
</form>
</div><!---->

</div>
<script type="text/javascript" src="indexScript.js"></script>
<script src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
</body>

<!--Citation for bootstrap-->
<!--code.tutsplus.com/tutorials/build-a-neat-html5-powered-contact-form\-\-net-20426-->
<!--Resume sidebar-->
<!--http://startbootstrap.com/templates/simple-sidebar.html-->

</html>
