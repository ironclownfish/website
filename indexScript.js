var uoclasses = [];
var reqs = {
phys  : [],
math  : [],
cis   : [],
gen   : [],
length: 4
}
var wheelMatrix = [];
var loaded = [false, false];
var curFade = 1;

$('#homediv').ready(function() {
	var $homediv = $('#homediv');
	var $spacer = $('<div></div>');
	$spacer.attr('id', 'homespacer');
	$homediv.height($(window).height());
	$spacer.height($homediv.height()-$('#homelayer1').height()-$('#homelayer2').height());
	$homediv.prepend($spacer);
        $('#homenav').click(function() {
            $('#topnavdiv .active').attr('class','');
            $('#homenav').addClass('active');
            $('#resumediv, #chart, #projectsdiv, #interestsdiv, #contactdiv').fadeOut(133, function() {
                $('#homediv').delay(133).fadeIn(133)});
            });
        $('#resumenav').click(function() {
            $('#topnavdiv .active').attr('class','');
            $('#resumenav').addClass('active');
            $('#homediv, #chart, #projectsdiv, #interestsdiv, #contactdiv').fadeOut(133, function() {
                $('#resumediv').delay(133).fadeIn(133)});
            });
	
	$('#edunav').click(function() {
	    $('#topnavdiv .active').attr('class','');
	    $('#edunav').addClass('active');
            $('#homediv, #resumediv, #projectsdiv, #interestsdiv, #contactdiv').fadeOut(133, function() {
	        $('#chart').delay(133).fadeIn(133)}); 
	});

        $('#projectsnav').click(function() {
            $('#topnavdiv .active').attr('class','');
            $('#projectsnav').addClass('active');
            $('#homediv, #resumediv, #chart, #interestsdiv, #contactdiv').fadeOut(133, function() {
                $('#projectsdiv').delay(133).fadeIn(133)});
            });
	$('#interestsnav').click(function() {
            $('#topnavdiv .active').attr('class','');
            $('#interestsnav').addClass('active');
            $('#homediv, #resumediv, #chart, #projectsdiv, #contactdiv').fadeOut(133, function() {
	    $('#interestsdiv').delay(133).fadeIn(133)});
	    alert("This section is in progress :)");
	    });
        $('#contactnav').click(function() {
            $('#topnavdiv .active').attr('class','');
            $('#contactnav').addClass('active');
            $('#homediv, #resumediv, #chart, #interestsdiv, #projectsdiv').fadeOut(133, function() {
                $('#contactdiv').delay(133).fadeIn(133)});
            });
	var ihash = $(location).attr('href').indexOf('#') 
	if (ihash > -1)
	  $($(location).attr('href').slice(ihash)+'nav').click();
	//Home parallax
	$homelayer2 = $('#homelayer2');
	width = $(document).width();
	$(document).mousemove(function(e){
	  var offset = $homelayer2.offset();
	  $homelayer2.offset({top:offset.top , left:(e.pageX/60 - 120)});
	});
});
$('#projectsdiv').ready(function() {
	//Projects page
	$('#reuprojnav').click(function() {
	    $('#projectsnav .active').attr('class','');
	    $('#reuprojnav').addClass('active');
	    $('#shelldiv, #chessdiv, #fpgadiv, #spacediv, #websitediv').fadeOut(133, function() {
	        $('#ontologydiv').delay(133).fadeIn(133)});
	});

	$('#chessprojnav').click(function() {
	    $('#projectsnav .active').attr('class','');
	    $('#chessprojnav').addClass('active');
	    $('#shelldiv, #ontologydiv, #fpgadiv, #spacediv, #websitediv').fadeOut(133, function() {
	        $('#chessdiv').delay(133).fadeIn(133)});
	});
	
	$('#fpgaprojnav').click(function() {
	    $('#projectsnav .active').attr('class','');
	    $('#fpgaprojnav').addClass('active');
	    $('#shelldiv, #ontologydiv, #chessdiv, #spacediv, #websitediv').fadeOut(133, function() {
	        $('#fpgadiv').delay(133).fadeIn(133)});
	});

	$('#shellprojnav').click(function() {
	    $('#projectsnav .active').attr('class','');
	    $('#shellprojnav').addClass('active');
	    $('#ontologydiv, #chessdiv, #fpgadiv, #spacediv, #websitediv').fadeOut(133, function() {
	        $('#shelldiv').delay(133).fadeIn(133)});
	});
	
	$('#spaceprojnav').click(function() {
	    $('#projectsnav .active').attr('class','');
	    $('#spaceprojnav').addClass('active');
	    $('#ontologydiv, #chessdiv, #fpgadiv, #websitediv, #shelldiv').fadeOut(133, function() {
	        $('#spacediv').delay(133).fadeIn(133)});
	});

	$('#websiteprojnav').click(function() {
	    $('#projectsnav .active').attr('class','');
	    $('#websiteprojnav').addClass('active');
	    $('#ontologydiv, #chessdiv, #fpgadiv, #spacediv, #shelldiv').fadeOut(133, function() {
	        $('#websitediv').delay(133).fadeIn(133)});
	});
});
//Load requirements data, then load UO classes data.
$.getJSON('degreereqs.json', function(data) {
        reqs.phys = data.physreqs;
        reqs.math = data.mathreqs;
        reqs.cis  = data.cisreqs;
        reqs.gen  = data.genreqs;
        loaded[0] = true;
        makewheel(loaded);
        });

//Load data about UO classes.
$.getJSON('curriculum.json', function(data) {
        uoclasses = data.uoclasses;
        loaded[1] = true;
        makewheel(loaded);
        });

function makewheel(ready) {
    //make sure everything is loaded
    for (var i in ready) if (!ready[i]) return false;
    //Make adjacency matrix for course wheel. 
    for (var i = 0; i < reqs.length; i++)
        wheelMatrix.push([]);
    for (var i in uoclasses) {
        var row = Array.apply(null, new Array(uoclasses.length+reqs.length))
            .map(Number.prototype.valueOf,0);
        for (var r = 0; r < reqs.length; r++)
            wheelMatrix[r][i] = 0;
        for (var j in reqs.cis) {
            if (reqs.cis[j] == uoclasses[i].subject+" "+uoclasses[i].course)
                wheelMatrix[0][i]+=1;
        }
        for (var j in reqs.phys) {
            if (reqs.phys[j] == uoclasses[i].subject+" "+uoclasses[i].course)
                wheelMatrix[1][i]+=1;
        }
        for (var j in reqs.math) {
            if (reqs.math[j] == uoclasses[i].subject+" "+uoclasses[i].course)
                wheelMatrix[2][i]+=1;
        }
        for (var j in reqs.gen) {
            if (reqs.gen[j] == uoclasses[i].subject+" "+uoclasses[i].course)
                wheelMatrix[3][i]+=1;
        }
        wheelMatrix.push(row);
    }
    for (var c = 0; c < reqs.length; c++) {
        for (var r = 0; r < reqs.length; r++) {
            wheelMatrix[c].unshift(0);
        }
    }
    //Make course wheel.
    var chord = d3.layout.chord()
        .padding(.05)
        //    .sortSubgroups(d3.descending)
        .matrix(wheelMatrix);

    var w = 500,
        h = 500,
        r0 = Math.min(w, h) * .41,
        r1 = r0 * 1.1;

    var fill = d3.scale.ordinal()
        .domain(d3.range(4))
        .range(["#000000", "#FFDD89", "#957244", "#F26223"]);
//	.range(['','','','']);
    var svg = d3.select("#chart")
        .append("svg:svg")
        .attr("width", 1.8*w)
        .attr("height", 1.8*h)
        .append("svg:g")
        .attr("transform", "translate(" + 0.9*w + "," + 0.9*h + ")");

    svg.append("svg:g")
        .selectAll("path")
        .data(chord.groups)
        .enter().append("svg:path")
        .style("fill", function(d) { return fill(d.index); })
        .style("stroke", function(d) { return fill(d.index); })
        .attr("d", d3.svg.arc().innerRadius(r0).outerRadius(r1)
                .endAngle(function(d) { //thicken sector
                    return d.endAngle - d.startAngle > .03?
                    d.endAngle : d.endAngle+.015;
                    })
                .startAngle(function(d) {     
                    return d.endAngle - d.startAngle > .03?
                    d.startAngle : d.startAngle-.015;
                    })) 
    .on("mouseover", fade(0))//.1))
        .on("touchstart", fade(1-curFade))
        .on("mouseout", fade(1));
    //    .on("touchleave", fade(1));

    var ticks = svg.append("svg:g")
        .selectAll("g")
        .data(chord.groups)
        .enter().append("svg:g")
        .selectAll("g")
        .data(groupTicks)
        .enter().append("svg:g")
        .attr("transform", function(d) {
                return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
                + "translate(" + r1 + ",0)";
                });

    ticks.append("svg:text")
        .style({
                'font-family' : "Verdana",
                'font-size'   : "8px",
                'text-rendering' : "optimizeLegibility"
                //'fill'  : "#444444"
                //'opacity'     : "0.4"
                })
    .attr("x", 8)
        .attr("dy", ".35em")
        .attr("text-anchor", function(d) {
                return d.angle > Math.PI ? "end" : null;
                })
    .attr("transform", function(d) {
            return d.angle > Math.PI ? "rotate(180)translate(-16)" : null;
            })
    .text(function(d) { return d.label; });

    svg.append("svg:g")
        //.style('shape-rendering','geometricPrecision')
        .attr("class", "chord")
        .selectAll("path")
        .data(chord.chords)
        .enter().append("svg:path")
        .style("fill", function(d) { return fill(d.target.index); })
        .attr("d", d3.svg.chord().radius(r0)
                .endAngle(function(d) { //thicken end of chord
                    return d.endAngle - d.startAngle > .03?
                    d.endAngle : d.endAngle+.018;
                    })
                .startAngle(function(d) {     
                    return d.endAngle - d.startAngle > .03?
                    d.startAngle : d.startAngle-.018;
                    }))
    .style("opacity", 1);

    /** Returns an array of tick angles and labels, given a group. */
    function groupTicks(d) { 
        return [{
index: d.index,
           angle: (d.startAngle+d.endAngle)/2,
           label: d.index >= reqs.length?
               uoclasses[d.index-reqs.length].title
               : d.index == 0?"B.S. Computer and Information Science"
               : d.index == 1?"B.S. Physics"
               : d.index == 2?"Mathematics minor"
               : d.index == 3?"Robert D. Clark Honors College"
               : ""
               //}, {
               //angle: (d.startAngle+d.endAngle/2+.05),
               //label: d.index == 0?"Information Science":""
    }];
    }

    /** Returns an event handler for fading a given chord group. */
    function fade(opacity) {
        curFade = opacity;
        return function(g, i) {
            svg.selectAll("g.chord path")
                .filter(function(d) {
                        return d.source.index != i && d.target.index != i;
                        })
            .transition()
                .style("opacity", opacity);
            svg.selectAll("text")
                .filter(function(d) {
                        //Return true for all irrelevant text.
                        return d.index != i && wheelMatrix[i][d.index] == 0 && wheelMatrix[d.index][i] == 0;  
                        //return Math.abs(d.angle - (g.startAngle+g.endAngle)/2) > .04
                        }).transition().style("opacity", opacity);
        };
    }
}


